make > temp 2>&1 

make

dmesg -C

insmod "$1.ko"
rmmod "$1"

dmesg

errors=$(cat temp | grep "error")
echo -n "\033[31;42m "
echo -n"$errors"
echo -n "\033[0m"
rm temp
