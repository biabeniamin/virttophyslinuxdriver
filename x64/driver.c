#include <linux/module.h>
#include <linux/printk.h>
#include <linux/slab.h>
#include "driver_a.c"
typedef uint64_t QWORD, *PQWORD;
typedef uint32_t DWORD, *PDWORD;
typedef uint16_t WORD, *PWORD;
typedef uint8_t BYTE, *PBYTE;

QWORD GetNextLevel4KPaging(QWORD QWPrevLevelAddress,
	QWORD QWBitsLinearAddress)
{
	QWORD qwPhysAddress;
	PQWORD pqwVirtAddress;
	QWORD qwContent;

	qwPhysAddress=0;
	pqwVirtAddress=0;
	qwContent=0;

	qwPhysAddress = QWPrevLevelAddress;
	qwPhysAddress += QWBitsLinearAddress << 3;

	pqwVirtAddress = phys_to_virt(qwPhysAddress);

	

	qwContent = *pqwVirtAddress;

#ifdef DEBUG
	printk("PhysAddress: %llx \n",qwPhysAddress);	
	printk("VirtAddress: %llx \n",pqwVirtAddress);	
	printk("Content: %llx \n",qwContent);	
#endif
	return qwContent;
}

QWORD GetContentFromPageTable(QWORD QWPrevLevelAddress,
	QWORD QWBitsLinearAddress)
{
	QWORD qwPhysAddress;
	PQWORD pqwVirtAddress;
	QWORD qwContent;

	qwPhysAddress=0;
	pqwVirtAddress=0;
	qwContent=0;

	qwPhysAddress = QWPrevLevelAddress;
	qwPhysAddress += QWBitsLinearAddress;

	pqwVirtAddress = phys_to_virt(qwPhysAddress);

	

	qwContent = *pqwVirtAddress;

#ifdef DEBUG
	printk("Pte content: %llx \n",QWPrevLevelAddress);	
	printk("Linear: %llx \n",QWBitsLinearAddress);	
	printk("pte:r %llx \n",QWPrevLevelAddress);	
	printk("pte:r %llx \n", ((1ULL<<50)-1));	
	printk("pte:r %llx \n",(QWPrevLevelAddress &((1ULL<<50)-1)));	
	printk("PhysAddress: %llx \n",qwPhysAddress);	
	printk("VirtAddress: %llx \n",pqwVirtAddress);	
	printk("Content: %llx \n",qwContent);	
#endif
	return qwContent;
}

QWORD GetContentOfCR3(void)
{
	QWORD cr3PhysAddress;
	PQWORD cr3VirtAddress;
	QWORD cr3;
	QWORD cr3Content;

	cr3=native_read_cr3();

#ifdef DEBUG
	printk("----------\nCR3\n-----------\n");
	printk("cr3: %llx \n",cr3);

#endif

	return cr3;
}

QWORD GetContentOfPML4E(QWORD QWLinearAddress,
	QWORD QWCr3)
{
	QWORD qwPdptPhys;	

#ifdef DEBUG
	printk("----------\nPML4\n-----------\n");
#endif

	qwPdptPhys=GetNextLevel4KPaging(QWCr3 & (~((1<<12)-1)),
		(QWLinearAddress >> 39) & ((1 << 9)-1)
	);

	return qwPdptPhys;


}
QWORD GetContentOfPDPTE(QWORD QWLinearAddress,
	QWORD QWPml4e)
{
	QWORD qwPdPhys;	

	qwPdPhys=0;

#ifdef DEBUG
	printk("----------\nPDPT\n-----------\n");
#endif

	qwPdPhys=GetNextLevel4KPaging(QWPml4e & (~((1<<12)-1)) & ((1<<50)-1),
		(QWLinearAddress >> 30) & ((1 << 9)-1)
	);

	return qwPdPhys;
}
QWORD GetContentOfPDE(QWORD QWLinearAddress,
	QWORD QWPdpte)
{
	QWORD qwPtPhys;	

	qwPtPhys=0;

#ifdef DEBUG
	printk("----------\nPD\n-----------\n");
#endif

	qwPtPhys=GetNextLevel4KPaging(QWPdpte & (~((1<<12)-1)) & ((1<<50)-1),
		(QWLinearAddress >> 21) & ((1 << 9)-1)
	);

	return qwPtPhys;
}
QWORD GetContentOfPTE(QWORD QWLinearAddress,
	QWORD QWPt)
{
	QWORD qwPhysAddress;	

	qwPhysAddress=0;

#ifdef DEBUG
	printk("----------\nPT\n-----------\n");
#endif

	//qwPhysAddress=GetContentFromPageTable((QWPt & (~((1<<12)-1))) & ((1<<51)-1),
	//	QWLinearAddress & ((1 << 12)-1)
	//);
	qwPhysAddress=GetNextLevel4KPaging((QWPt & (~((1<<12)-1))) & ((1<<50)-1),
		(QWLinearAddress >> 12)& ((1 << 9)-1)
	);

	return qwPhysAddress;
}
QWORD GetPhysAddress(QWORD QWLinearAddress,
	QWORD QWPt)
{
	QWORD qwPhysAddress;	

	qwPhysAddress=0;

#ifdef DEBUG
	printk("----------\nPhys address\n-----------\n");
#endif

	qwPhysAddress=GetContentFromPageTable((QWPt & (~((1ULL<<12)-1))) & ((1ULL<<50)-1),
		QWLinearAddress & ((1 << 12)-1)
	);
	//qwPhysAddress=GetNextLevel4KPaging((QWPt & (~((1<<12)-1))) & ((1<<50)-1),
	//	(QWLinearAddress >> 12)& ((1 << 9)-1)
	//);

	return qwPhysAddress;
}
QWORD GetPhysAddress2MbPage(QWORD QWLinearAddress,
	QWORD QWPt)
{
	QWORD qwPhysAddress;	

	qwPhysAddress=0;

#ifdef DEBUG
	printk("----------\nPhys address\n-----------\n");
#endif

	qwPhysAddress=GetContentFromPageTable((QWPt & (~((1ULL<<21)-1))) & ((1ULL<<50)-1),
		QWLinearAddress & ((1 << 21)-1)
	);
	//qwPhysAddress=GetNextLevel4KPaging((QWPt & (~((1<<12)-1))) & ((1<<50)-1),
	//	(QWLinearAddress >> 12)& ((1 << 9)-1)
	//);

	return qwPhysAddress;
}
QWORD GetPhysAddress1GbPage(QWORD QWLinearAddress,
	QWORD QWPt)
{
	QWORD qwPhysAddress;	

	qwPhysAddress=0;

#ifdef DEBUG
	printk("----------\nPhys address\n-----------\n");
#endif

	qwPhysAddress=GetContentFromPageTable((QWPt & (~((1ULL<<30)-1))) & ((1ULL<<50)-1),
		QWLinearAddress & ((1 << 30)-1)
	);
	//qwPhysAddress=GetNextLevel4KPaging((QWPt & (~((1<<12)-1))) & ((1<<50)-1),
	//	(QWLinearAddress >> 12)& ((1 << 9)-1)
	//);

	return qwPhysAddress;
}
QWORD ConvertVirtToPhys(QWORD VirtAddress)
{
	QWORD qwCr3;
	QWORD qwPml4e;
	QWORD qwPdpte;
	QWORD qwPde;
	QWORD qwPte;
	QWORD qwPhys;
	ACCESS_RIGHT_TYPE artUserAccessRights;
	ACCESS_RIGHT_TYPE artSuperAccessRights;
	WORD wIs2MbPage;
	WORD wIs1GbPage;
	WORD wIsUserAddress;

	qwPte=0;
	wIs2MbPage=0;
	wIs1GbPage=0;
	wIsUserAddress=0;

#ifdef DEBUG
	printk("Linear address : %llx \n",VirtAddress);
	printk("Content of linear address : %llx \n",*((PQWORD)VirtAddress));
#endif
	artUserAccessRights=GetReadAccessRightsForUserMode();
	artSuperAccessRights=GetReadAccessRightsForSupervisor();
	qwCr3=GetContentOfCR3();

	qwPml4e=GetContentOfPML4E(VirtAddress,
		qwCr3);

	wIsUserAddress=qwPml4e >> 2 & 1;
	if(1 == wIsUserAddress &&  DependsRWFlag == artUserAccessRights)
	{
		if(qwPml4e >> 1 & 1 == 0)
		{
#ifdef DEBUG
			printk("RW flag is 0\n");
#endif
			goto error;
		}
	}

	if(1 == wIsUserAddress && NotAllowed == artUserAccessRights)
	{
#ifdef DEBUG
		printk("Cannot acces any usermode address\n");
#endif
		goto error;
	}

	qwPdpte=GetContentOfPDPTE(VirtAddress,
		qwPml4e);
	if(1 == wIsUserAddress &&  DependsRWFlag == artUserAccessRights)
	{
		if(qwPdpte >> 1 & 1 == 0)
		{
#ifdef DEBUG
			printk("RW flag is 0\n");
#endif
			goto error;
		}
	}
	wIs1GbPage=qwPdpte >> 7 & 1;

	if(1 == wIs1GbPage)
	{
#ifdef DEBUG
		printk("1 Gb page\n");
#endif
		qwPhys=GetPhysAddress1GbPage(VirtAddress,
			qwPdpte);
	}
	else
	{
		qwPde=GetContentOfPDE(VirtAddress,
			qwPdpte);
		wIs2MbPage = qwPde >> 7 & 1;

		if(1 == wIs2MbPage)
		{
	#ifdef DEBUG
			printk("2 Mb page\n");
	#endif
			qwPhys=GetPhysAddress2MbPage(VirtAddress,
				qwPde);
		}
		else
		{
	#ifdef DEBUG
			printk("4kb page\n");
	#endif
			qwPte=GetContentOfPTE(VirtAddress,
				qwPde);
			qwPhys=GetPhysAddress(VirtAddress,
				qwPte);
		}
	}
error:

	return qwPte;
}
static int __init test_driver_init(void)
{
        printk("init...\n");
	DWORD te=5;
	ConvertVirtToPhys(&printk);
	return 0;
}


static void __exit test_driver_exit (void)
{
        printk("Uninit...\n");
}

module_init(test_driver_init);
module_exit(test_driver_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("m");
MODULE_VERSION("0.2");

