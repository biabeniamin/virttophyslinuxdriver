
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/slab.h>

typedef uint64_t QWORD, *PQWORD;
typedef uint32_t DWORD, *PDWORD;
typedef uint16_t WORD, *PWORD;
typedef uint8_t BYTE, *PBYTE;

#define DEBUG

typedef enum
{
	NotAllowed=0,
	Allowed=1,
	AllowedWithProtKey=2,
	DependsRWFlag=3
} ACCESS_RIGHT_TYPE;
ACCESS_RIGHT_TYPE GetReadAccessRightsForUserMode(void)
{
	QWORD qwCr0;
	QWORD qwCr4;
	QWORD qwSmap;
	QWORD qwWp;
	QWORD qwEflgAc;
	QWORD qwPke;
	ACCESS_RIGHT_TYPE artAccessType;

	qwCr4=0;
	qwSmap=0;
	qwWp=0;
	qwEflgAc=0;
	qwPke=0;

	qwCr0=native_read_cr0();
	qwCr4=native_read_cr4();

	qwSmap = qwCr4 >> 21 & 1;
	qwWp = qwCr0 >> 16 &1;
	qwPke = qwCr4 >> 21 & 1;

	if(0 == qwSmap)
	{
		artAccessType=AllowedWithProtKey;
	}
	else
	{
		if(0 == qwEflgAc)
		{
			artAccessType=NotAllowed;
		}
		else
		{
			artAccessType=DependsRWFlag;
		}
	}

#ifdef DEBUG
	printk("----------\nAccessRights For usermode\n-----------\n");
	printk("%llx \n",qwCr4);
	printk("cr4.smap= %llx \n",qwSmap);
	printk("cr0.wp %llx \n",qwWp);
	printk("cr4.pke= %llx \n",qwPke);
	if( 0 == qwSmap)
	{
		printk("data readed with protenction key\n");
	}
	else if( 1 == qwSmap)
	{
		printk("data readed implicit or explicit \n");
	}

	if(0 == qwWp)
	{
		if(0 == qwSmap)
		{
			printk("data may be writted to any user-mode address with protenction key \n");
		}
		else
		{
			if(1 == qwEflgAc)
			{	
				printk("data may be writted to any user-mode address with protection key fow which access is permitted\n");
			}
			else
			{
			printk("data may not  be writted to any user-mode address \n");
			}
		}
	}
	else if(1 == qwWp)
	{
		if(0 == qwSmap)
		{
			printk("data may be writted to user-mode address if r/w flag is enabled \n");
		}
		else
		{
			if(1 == qwEflgAc)
			{	
				printk("data may be writted to user-mode address if r/w flag is enabled \n");
			}
			else
			{
			printk("data may not  be writted to any user-mode address \n");
			}
		}
	}
#endif
	
	return artAccessType;
}

ACCESS_RIGHT_TYPE GetReadAccessRightsForSupervisor(void)
{
	QWORD qwCr0;
	QWORD qwCr4;
	QWORD qwSmap;
	QWORD qwWp;
	QWORD qwEflgAc;
	QWORD qwPke;
	ACCESS_RIGHT_TYPE artAccessType;

	qwCr4=0;
	qwSmap=0;
	qwWp=0;
	qwEflgAc=0;
	qwPke=0;

	artAccessType=Allowed;

#ifdef DEBUG
	printk("----------\nAccessRights For supervisor addresses\n-----------\n");
	printk("is allowed");
	
#endif
	
	return artAccessType;
}
